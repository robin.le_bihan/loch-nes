# Loch NES

Nintendo Entertainment System (NES) emulator written in Rust.

## Why?
I made this project for educational purpose only. After programming an
Chip8 emulator, I wanted to go further in emulator programming. The goal
was also to practice a bit of Rust language.

## Resources

* 6502 CPU:
    * [masswerk.at](https://www.masswerk.at/6502/6502_instruction_set.html)
    * [6502.org](http://www.6502.org/tutorials/6502opcodes.html)
    * [Ultimate C64 Talk](https://www.youtube.com/watch?v=ZsRRCnque2E)

* Tutorials:
    * `javidx9`: [NES Emulator from scratch](https://www.youtube.com/watch?v=nViZg02IMQo&list=PLrOv9FMX8xJHqMvSGB_9G9nZZ_4IgteYf)

[NES technical specifications](http://nesdev.com/NESDoc.pdf)