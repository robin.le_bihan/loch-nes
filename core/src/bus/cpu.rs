use std::sync::{Arc, Mutex};

use crate::bus::Bus;
use crate::cartridge::Cartridge;
use crate::memory::{ReadMemory, WriteMemory};
use crate::memory::wram::WorkRAM;

pub struct CPUBus {
    wram: Arc<Mutex<WorkRAM>>,
    cartridge: Option<Arc<Mutex<Cartridge>>>,
}

impl CPUBus {
    pub fn new() -> Self {
        CPUBus {
            wram: Arc::new(Mutex::new(WorkRAM::new())),
            cartridge: None,
        }
    }

    pub fn connect_cartridge(&mut self, cartridge: Cartridge) {
        self.cartridge = Some(Arc::new(Mutex::new(cartridge)))
    }

    pub fn ready(&self) -> bool {
        self.cartridge.is_some()
    }

    pub fn wram(&self) -> Arc<Mutex<WorkRAM>> {
        self.wram.clone()
    }
}

impl Bus for CPUBus {
    fn read_byte(&self, addr: u16) -> u8 {
        match addr {
            0x0..=0x1FFF => {
                self.wram
                    .lock()
                    .unwrap()
                    .read(addr)
            }
            0x8000..=0xFFFF => {
                self.cartridge
                    .as_ref()
                    .expect("No cartridge inserted")
                    .lock()
                    .unwrap()
                    .cpu_read(addr)
            }
            _ => panic!("Unmapped memory address"),
        }
    }

    fn write_byte(&mut self, addr: u16, byte: u8) {
        match addr {
            0x0..=0x1FFF => {
                self.wram
                    .lock()
                    .unwrap()
                    .write(addr, byte)
            }
            0x8000..=0xFFFF => {
                panic!("Cartridge is a Read-Only-Memory")
            }
            _ => panic!("Unmapped memory address"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn read_byte_wram() {
        let bus = CPUBus::new();
        bus.wram
            .lock()
            .unwrap()
            .write(0x0231, 0x23);
        assert_eq!(bus.read_byte(0x0231), 0x23);
    }

    #[test]
    fn write_byte_wram() {
        let mut bus = CPUBus::new();
        bus.write_byte(0x0231, 0x23);
        assert_eq!(
            bus.wram
                .lock()
                .unwrap()
                .read(0x0231), 0x23
        );
    }

    #[test]
    fn read_word_wram() {
        let bus = CPUBus::new();
        bus.wram.lock().unwrap().write(0x0232, 0x23);
        bus.wram.lock().unwrap().write(0x0231, 0x12);
        assert_eq!(bus.read_word(0x0231), 0x2312);
    }

    #[test]
    fn write_word_wram() {
        let mut bus = CPUBus::new();
        bus.write_word(0x0231, 0x2312);
        assert_eq!(bus.wram.lock().unwrap().read(0x0232), 0x23);
        assert_eq!(bus.wram.lock().unwrap().read(0x0231), 0x12);
    }
}
