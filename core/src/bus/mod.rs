/// Represents a data bus.
pub trait Bus {
    fn read_byte(&self, addr: u16) -> u8;
    fn write_byte(&mut self, addr: u16, byte: u8);

    fn read_word(&self, addr: u16) -> u16 {
        let byte1 = self.read_byte(addr + 1) as u16;
        let byte2 = self.read_byte(addr) as u16;

        (byte1 << 8) | byte2
    }

    fn write_word(&mut self, addr: u16, word: u16) {
        self.write_byte(addr + 1, ((word & 0xFF00) >> 8) as u8);
        self.write_byte(addr, (word & 0x00FF) as u8);
    }
}

pub mod cpu;
