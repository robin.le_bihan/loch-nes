use std::fs::File;
use std::io;
use std::io::Read;

use crate::cartridge::{Cartridge, Header};
use crate::cartridge::mapper;

#[derive(Debug)]
pub enum CartridgeError {
    IOError(io::Error),
    InvalidHeaderName,
}

impl std::convert::From<io::Error> for CartridgeError {
    fn from(err: io::Error) -> CartridgeError {
        CartridgeError::IOError(err)
    }
}

impl Header {
    pub fn new() -> Self {
        Header {
            trainer: false,
            mapper_id: 0,
            prg_size: 0,
            chr_size: 0,
            mirroring: false,
            prg_ram: false,
            ignore_mirroring: false,
            ram_size: 0,
        }
    }
}

static PRG_BANK_SIZE: usize = (1 << 14);
static CHR_BANK_SIZE: usize = (1 << 13);

impl Cartridge {
    pub fn from_file(path: &str) -> Result<Self, CartridgeError> {
        let mut file = File::open(path)?;
        let mut buffer = Vec::new();

        file.read_to_end(&mut buffer)?;

        Cartridge::from_bytes(&buffer[..])
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<Self, CartridgeError> {
        let header = Cartridge::read_header(&bytes[..16])?;
        let mut offset = 16;

        if header.trainer {
            offset += 512;
        }

        let prg_total_size = header.prg_size as usize * PRG_BANK_SIZE;
        let mut prg_rom = bytes[offset..(offset + prg_total_size)].to_vec();
        prg_rom.push(0xFF);
        offset += prg_total_size;

        let chr_total_size = header.chr_size as usize * CHR_BANK_SIZE;
        let chr_rom = bytes[offset..(offset + chr_total_size)].to_vec();

        Ok(
            Cartridge {
                chr_rom,
                prg_rom,
                mapper: Box::new(
                    mapper::from_id(
                        header.mapper_id,
                        header.prg_size,
                        header.chr_size,
                    )
                ),
                header,
            }
        )
    }

    fn read_header(bytes: &[u8]) -> Result<Header, CartridgeError> {
        let name = &bytes[..4];
        if *name != [0x4E, 0x45, 0x53, 0x1A] {
            return Err(CartridgeError::InvalidHeaderName);
        }

        let mut header = Header::new();

        header.prg_size = bytes[4];
        header.chr_size = bytes[5];
        header.mapper_id = (bytes[7] & 0xF0) | ((bytes[6] & 0xF0) >> 4);
        header.ram_size = bytes[8];

        Ok(header)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn invalid_header_name() {
        let mut bytes = [0x0; 16];
        bytes[0] = 0x4E;
        bytes[1] = 0x45;
        bytes[2] = 0x53;
        bytes[3] = 0x1B;
        let res = Cartridge::read_header(&bytes[..]);
        assert!(res.is_err(), "InvalidHeaderName error is expected.");
    }

    #[test]
    fn header_parse() {
        let bytes = [
            0x4e, 0x45, 0x53, 0x1a, 0x02, 0x01, 0x01, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        ];

        let header = Cartridge::read_header(&bytes[..]).unwrap();
        assert_eq!(header.prg_size, 2);
        assert_eq!(header.chr_size, 1);
        assert_eq!(header.mapper_id, 0);
        assert_eq!(header.ram_size, 0);
    }
}
