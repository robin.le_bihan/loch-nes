use crate::cartridge::mapper::Mapper;

pub struct Mapper0 {
    prg_size: u8,
    _chr_size: u8,
}

impl Mapper for Mapper0 {
    fn cpu_map(&self, addr: u16) -> u16 {
        let mask = if self.prg_size > 1 {
            0x7FFF
        } else {
            0x3FFF
        };

        match addr {
            0x8000..=0xFFFF => (mask & addr),
            _ => panic!("Unmapped memory address"),
        }
    }

    fn ppu_map(&self, addr: u16) -> u16 {
        match addr {
            0x0000..=0x1FFF => addr,
            _ => panic!("Unmapped memory address"),
        }
    }

    fn name(&self) -> String {
        "NROM".to_owned()
    }
}

impl Mapper0 {
    pub fn new(prg_size: u8, chr_size: u8) -> Self {
        Mapper0 {
            prg_size,
            _chr_size: chr_size,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn cpu_map_1_bank() {
        let mapper = Mapper0::new(1, 0);
        let addr = mapper.cpu_map(0x8723);
        assert_eq!(addr, 0x723);
    }

    #[test]
    fn cpu_map_1_bank_mirror() {
        let mapper = Mapper0::new(1, 0);
        let addr = mapper.cpu_map(0xDFAB);
        assert_eq!(addr, 0x1FAB);
    }

    #[test]
    fn cpu_map_2_banks() {
        let mapper = Mapper0::new(2, 0);
        let addr = mapper.cpu_map(0xDFAB);
        assert_eq!(addr, 0x5FAB);
    }

    #[test]
    fn ppu_map() {
        let mapper = Mapper0::new(1, 1);
        let addr = mapper.ppu_map(0x1234);
        assert_eq!(addr, 0x1234);
    }
}
