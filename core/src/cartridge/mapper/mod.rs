pub mod mapper0;

pub trait Mapper {
    fn cpu_map(&self, addr: u16) -> u16;
    fn ppu_map(&self, addr: u16) -> u16;
    fn name(&self) -> String;
}

pub fn from_id(id: u8, prg_banks: u8, chr_banks: u8) -> impl Mapper {
    match id {
        0 => mapper0::Mapper0::new(prg_banks, chr_banks),
        _ => panic!("Unknown mapper ID"),
    }
}
