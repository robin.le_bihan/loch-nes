use std::fmt;

mod mapper;
pub mod loader;

pub struct Header {
    pub trainer: bool,
    pub mapper_id: u8,
    pub prg_size: u8,
    pub chr_size: u8,
    pub mirroring: bool,
    pub prg_ram: bool,
    pub ignore_mirroring: bool,
    pub ram_size: u8,
}

pub struct Cartridge {
    pub mapper: Box<dyn mapper::Mapper + Send>,
    pub prg_rom: Vec<u8>,
    pub chr_rom: Vec<u8>,
    pub header: Header,
}

impl Cartridge {
    pub fn new() -> Self {
        Cartridge {
            mapper: Box::new(mapper::from_id(0, 0, 0)),
            prg_rom: vec![0xFF],
            chr_rom: vec![],
            header: Header::new(),
        }
    }

    pub fn cpu_read(&self, addr: u16) -> u8 {
        let real_addr = self.mapper.cpu_map(addr);
        self.prg_rom[real_addr as usize]
    }

    pub fn ppu_read(&self, addr: u16) -> u8 {
        let real_addr = self.mapper.ppu_map(addr);
        self.chr_rom[real_addr as usize]
    }
}

impl fmt::Display for Cartridge {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Mapper type: {}, PRG-ROM: {}x16k, CHR-ROM: {}x8k",
               self.mapper.name(),
               self.header.prg_size,
               self.header.chr_size)
    }
}
