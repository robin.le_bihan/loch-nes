use crate::bus::Bus;
use crate::cpu::CPU;
use crate::cpu::instructions::{AddressMode, Instruction, Mnemonic};

#[derive(Debug)]
pub enum DecodeError {
    InvalidOpcode(u8),
}

impl CPU {
    /// Read a byte from bus at PC and increment PC by 1.
    fn read_byte<B: Bus>(&mut self, bus: &mut B) -> u8 {
        let byte = bus.read_byte(self.pc);
        self.pc += 1;
        byte
    }

    /// Read a word from bus at PC and increment PC by 2.
    fn read_word<B: Bus>(&mut self, bus: &mut B) -> u16 {
        let word = bus.read_word(self.pc);
        self.pc += 2;
        word
    }

    /// Decode the instruction pointed to by the PC.
    pub fn decode<B: Bus>(&mut self, bus: &mut B) -> Result<Instruction, DecodeError> {
        let opcode = self.read_byte(bus);

        if opcode == 0xFF {
            return Ok(Instruction::new(XXX, Implied));
        }

        let a = (opcode & (7 << 5)) >> 5;
        let b = (opcode & (7 << 2)) >> 2;
        let c = opcode & 3;

        use Mnemonic::*;
        use AddressMode::*;

        match (c, b) {
            (0, 0) => {
                match a {
                    0 => Ok(Instruction::new(BRK, Implied)),
                    1 => {
                        let operand = self.read_word(bus);
                        Ok(Instruction::new(JSR, Absolute(operand)))
                    }
                    5 | 6 | 7 => {
                        let operand = self.read_byte(bus);
                        match a {
                            5 => Ok(Instruction::new(LDY, Immediate(operand))),
                            6 => Ok(Instruction::new(CPY, Immediate(operand))),
                            7 => Ok(Instruction::new(CPX, Immediate(operand))),
                            _ => Err(DecodeError::InvalidOpcode(opcode))
                        }
                    }
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (0, 1) => {
                let operand = self.read_byte(bus);
                match a {
                    4 => Ok(Instruction::new(STY, Zeropage(operand))),
                    5 => Ok(Instruction::new(LDY, Zeropage(operand))),
                    6 => Ok(Instruction::new(CPY, Zeropage(operand))),
                    7 => Ok(Instruction::new(CPX, Zeropage(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (0, 2) => {
                match a {
                    0 => Ok(Instruction::new(PHP, Implied)),
                    1 => Ok(Instruction::new(PLP, Implied)),
                    2 => Ok(Instruction::new(PHA, Implied)),
                    3 => Ok(Instruction::new(PLA, Implied)),
                    4 => Ok(Instruction::new(DEY, Implied)),
                    5 => Ok(Instruction::new(TAY, Implied)),
                    6 => Ok(Instruction::new(INY, Implied)),
                    7 => Ok(Instruction::new(INX, Implied)),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (0, 3) => {
                let operand = self.read_word(bus);
                match a {
                    2 => Ok(Instruction::new(JMP, Absolute(operand))),
                    4 => Ok(Instruction::new(STY, Absolute(operand))),
                    3 => Ok(Instruction::new(JMP, Indirect(operand))),
                    5 => Ok(Instruction::new(LDY, Absolute(operand))),
                    6 => Ok(Instruction::new(CPY, Absolute(operand))),
                    7 => Ok(Instruction::new(CPX, Absolute(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (0, 4) => {
                let operand = self.read_byte(bus);
                match a {
                    0 => Ok(Instruction::new(BPL, Relative(operand))),
                    1 => Ok(Instruction::new(BMI, Relative(operand))),
                    2 => Ok(Instruction::new(BVC, Relative(operand))),
                    3 => Ok(Instruction::new(BVS, Relative(operand))),
                    4 => Ok(Instruction::new(BCC, Relative(operand))),
                    5 => Ok(Instruction::new(BCS, Relative(operand))),
                    6 => Ok(Instruction::new(BNE, Relative(operand))),
                    7 => Ok(Instruction::new(BEQ, Relative(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (0, 5) => {
                let operand = self.read_byte(bus);
                match a {
                    4 => Ok(Instruction::new(STY, ZeropageX(operand))),
                    5 => Ok(Instruction::new(LDY, ZeropageX(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (0, 6) => {
                match a {
                    0 => Ok(Instruction::new(CLC, Implied)),
                    1 => Ok(Instruction::new(SEC, Implied)),
                    2 => Ok(Instruction::new(CLI, Implied)),
                    3 => Ok(Instruction::new(SEI, Implied)),
                    4 => Ok(Instruction::new(TYA, Implied)),
                    5 => Ok(Instruction::new(CLV, Implied)),
                    6 => Ok(Instruction::new(CLD, Implied)),
                    7 => Ok(Instruction::new(SED, Implied)),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (0, 7) => {
                let operand = self.read_word(bus);
                match a {
                    5 => Ok(Instruction::new(LDY, AbsoluteX(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (1, 0) => {
                let operand = self.read_byte(bus);
                match a {
                    0 => Ok(Instruction::new(ORA, IndirectX(operand))),
                    2 => Ok(Instruction::new(EOR, IndirectX(operand))),
                    4 => Ok(Instruction::new(STA, IndirectX(operand))),
                    5 => Ok(Instruction::new(LDA, IndirectX(operand))),
                    6 => Ok(Instruction::new(CMP, IndirectX(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (1, 1) => {
                let operand = self.read_byte(bus);
                match a {
                    0 => Ok(Instruction::new(ORA, Zeropage(operand))),
                    2 => Ok(Instruction::new(EOR, Zeropage(operand))),
                    4 => Ok(Instruction::new(STA, Zeropage(operand))),
                    5 => Ok(Instruction::new(LDA, Zeropage(operand))),
                    6 => Ok(Instruction::new(CMP, Zeropage(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (1, 2) => {
                let operand = self.read_byte(bus);
                match a {
                    0 => Ok(Instruction::new(ORA, Immediate(operand))),
                    2 => Ok(Instruction::new(EOR, Immediate(operand))),
                    5 => Ok(Instruction::new(LDA, Immediate(operand))),
                    6 => Ok(Instruction::new(CMP, Immediate(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (1, 3) => {
                let operand = self.read_word(bus);
                match a {
                    0 => Ok(Instruction::new(ORA, Absolute(operand))),
                    2 => Ok(Instruction::new(EOR, Absolute(operand))),
                    4 => Ok(Instruction::new(STA, Absolute(operand))),
                    5 => Ok(Instruction::new(LDA, Absolute(operand))),
                    6 => Ok(Instruction::new(CMP, Absolute(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (1, 4) => {
                let operand = self.read_byte(bus);
                match a {
                    0 => Ok(Instruction::new(ORA, IndirectY(operand))),
                    2 => Ok(Instruction::new(EOR, IndirectY(operand))),
                    4 => Ok(Instruction::new(STA, IndirectY(operand))),
                    5 => Ok(Instruction::new(LDA, IndirectY(operand))),
                    6 => Ok(Instruction::new(CMP, IndirectY(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (1, 5) => {
                let operand = self.read_byte(bus);
                match a {
                    0 => Ok(Instruction::new(ORA, ZeropageX(operand))),
                    2 => Ok(Instruction::new(EOR, ZeropageX(operand))),
                    4 => Ok(Instruction::new(STA, ZeropageX(operand))),
                    5 => Ok(Instruction::new(LDA, ZeropageX(operand))),
                    6 => Ok(Instruction::new(CMP, ZeropageX(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (1, 6) => {
                let operand = self.read_word(bus);
                match a {
                    0 => Ok(Instruction::new(ORA, AbsoluteY(operand))),
                    2 => Ok(Instruction::new(EOR, AbsoluteY(operand))),
                    4 => Ok(Instruction::new(STA, AbsoluteY(operand))),
                    5 => Ok(Instruction::new(LDA, AbsoluteY(operand))),
                    6 => Ok(Instruction::new(CMP, AbsoluteY(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (1, 7) => {
                let operand = self.read_word(bus);
                match a {
                    0 => Ok(Instruction::new(ORA, AbsoluteX(operand))),
                    2 => Ok(Instruction::new(EOR, AbsoluteX(operand))),
                    4 => Ok(Instruction::new(STA, AbsoluteX(operand))),
                    5 => Ok(Instruction::new(LDA, AbsoluteX(operand))),
                    6 => Ok(Instruction::new(CMP, AbsoluteX(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (2, 0) => {
                let operand = self.read_byte(bus);
                match a {
                    5 => Ok(Instruction::new(LDX, Immediate(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (2, 1) => {
                let operand = self.read_byte(bus);
                match a {
                    0 => Ok(Instruction::new(ASL, Zeropage(operand))),
                    1 => Ok(Instruction::new(ROL, Zeropage(operand))),
                    2 => Ok(Instruction::new(LSR, Zeropage(operand))),
                    3 => Ok(Instruction::new(ROR, Zeropage(operand))),
                    4 => Ok(Instruction::new(STX, Zeropage(operand))),
                    5 => Ok(Instruction::new(LDX, Zeropage(operand))),
                    6 => Ok(Instruction::new(DEC, Zeropage(operand))),
                    7 => Ok(Instruction::new(INC, Zeropage(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (2, 2) => {
                match a {
                    0 => Ok(Instruction::new(ASL, Accumulator)),
                    1 => Ok(Instruction::new(ROL, Accumulator)),
                    2 => Ok(Instruction::new(LSR, Accumulator)),
                    3 => Ok(Instruction::new(ROR, Accumulator)),
                    4 => Ok(Instruction::new(TXA, Implied)),
                    5 => Ok(Instruction::new(TAX, Implied)),
                    6 => Ok(Instruction::new(DEX, Implied)),
                    7 => Ok(Instruction::new(NOP, Implied)),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (2, 3) => {
                let operand = self.read_word(bus);
                match a {
                    0 => Ok(Instruction::new(ASL, Absolute(operand))),
                    1 => Ok(Instruction::new(ROL, Absolute(operand))),
                    2 => Ok(Instruction::new(LSR, Absolute(operand))),
                    3 => Ok(Instruction::new(ROR, Absolute(operand))),
                    4 => Ok(Instruction::new(STX, Absolute(operand))),
                    5 => Ok(Instruction::new(LDX, Absolute(operand))),
                    6 => Ok(Instruction::new(DEC, Absolute(operand))),
                    7 => Ok(Instruction::new(INC, Absolute(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (2, 5) => {
                let operand = self.read_byte(bus);
                match a {
                    0 => Ok(Instruction::new(ASL, ZeropageX(operand))),
                    1 => Ok(Instruction::new(ROL, ZeropageX(operand))),
                    2 => Ok(Instruction::new(LSR, ZeropageX(operand))),
                    3 => Ok(Instruction::new(ROR, ZeropageX(operand))),
                    4 => Ok(Instruction::new(STX, ZeropageY(operand))),
                    5 => Ok(Instruction::new(LDX, ZeropageY(operand))),
                    6 => Ok(Instruction::new(DEC, ZeropageX(operand))),
                    7 => Ok(Instruction::new(INC, ZeropageX(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (2, 6) => {
                match a {
                    4 => Ok(Instruction::new(TXS, Implied)),
                    5 => Ok(Instruction::new(TSX, Implied)),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            (2, 7) => {
                let operand = self.read_word(bus);
                match a {
                    0 => Ok(Instruction::new(ASL, AbsoluteX(operand))),
                    1 => Ok(Instruction::new(ROL, AbsoluteX(operand))),
                    2 => Ok(Instruction::new(LSR, AbsoluteX(operand))),
                    3 => Ok(Instruction::new(ROR, AbsoluteX(operand))),
                    5 => Ok(Instruction::new(LDX, AbsoluteY(operand))),
                    6 => Ok(Instruction::new(DEC, AbsoluteX(operand))),
                    7 => Ok(Instruction::new(INC, AbsoluteX(operand))),
                    _ => Err(DecodeError::InvalidOpcode(opcode)),
                }
            }
            _ => Err(DecodeError::InvalidOpcode(opcode)),
        }
    }
}

#[cfg(test)]
mod test {
    use AddressMode::*;
    use Mnemonic::*;

    use crate::cpu::mocking::*;

    use super::*;

    fn test_decode_imply(opcode: u8, exp_instr: Instruction) {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        bus_mock.read_byte
            .given(Matcher::Val(0x0))
            .will_return(opcode);

        let instr = cpu.decode(&mut bus_mock).unwrap();
        assert_eq!(cpu.pc, 0x1 as u16);
        assert_eq!(instr, exp_instr);
    }

    fn test_decode_byte(opcode: u8, byte: u8, exp_instr: Instruction) {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        bus_mock.read_byte
            .given(Matcher::Val(0x0))
            .will_return(opcode);
        bus_mock.read_byte
            .given(Matcher::Val(0x1))
            .will_return(byte);

        let instr = cpu.decode(&mut bus_mock).unwrap();
        assert_eq!(cpu.pc, 0x2 as u16);
        assert_eq!(instr, exp_instr);
    }

    fn test_decode_word(opcode: u8, word: u16, exp_instr: Instruction) {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        bus_mock.read_byte
            .given(Matcher::Val(0x0))
            .will_return(opcode);
        bus_mock.read_word
            .given(Matcher::Val(0x1))
            .will_return(word);

        let instr = cpu.decode(&mut bus_mock).unwrap();
        assert_eq!(cpu.pc, 0x3 as u16);
        assert_eq!(instr, exp_instr);
    }

    #[test]
    fn decode_ror() {
        test_decode_imply(
            0x6A,
            Instruction::new(ROR, Accumulator),
        );
        test_decode_byte(
            0x66, 0x44,
            Instruction::new(ROR, Zeropage(0x44)),
        );
        test_decode_byte(
            0x76, 0x44,
            Instruction::new(ROR, ZeropageX(0x44)),
        );
        test_decode_word(
            0x6E, 0x4400,
            Instruction::new(ROR, Absolute(0x4400)),
        );
        test_decode_word(
            0x7E, 0x4400,
            Instruction::new(ROR, AbsoluteX(0x4400)),
        );
    }


    #[test]
    fn decode_rol() {
        test_decode_imply(
            0x2A,
            Instruction::new(ROL, Accumulator),
        );
        test_decode_byte(
            0x26, 0x44,
            Instruction::new(ROL, Zeropage(0x44)),
        );
        test_decode_byte(
            0x36, 0x44,
            Instruction::new(ROL, ZeropageX(0x44)),
        );
        test_decode_word(
            0x2E, 0x4400,
            Instruction::new(ROL, Absolute(0x4400)),
        );
        test_decode_word(
            0x3E, 0x4400,
            Instruction::new(ROL, AbsoluteX(0x4400)),
        );
    }

    #[test]
    fn decode_sty() {
        test_decode_byte(
            0x84, 0x44,
            Instruction::new(STY, Zeropage(0x44)),
        );
        test_decode_byte(
            0x94, 0x44,
            Instruction::new(STY, ZeropageX(0x44)),
        );
        test_decode_word(
            0x8C, 0x4400,
            Instruction::new(STY, Absolute(0x4400)),
        );
    }

    #[test]
    fn decode_stx() {
        test_decode_byte(
            0x86, 0x44,
            Instruction::new(STX, Zeropage(0x44)),
        );
        test_decode_byte(
            0x96, 0x44,
            Instruction::new(STX, ZeropageY(0x44)),
        );
        test_decode_word(
            0x8E, 0x4400,
            Instruction::new(STX, Absolute(0x4400)),
        );
    }

    #[test]
    fn decode_tay() {
        test_decode_imply(
            0xA8,
            Instruction::new(TAY, Implied),
        );
    }

    #[test]
    fn decode_tya() {
        test_decode_imply(
            0x98,
            Instruction::new(TYA, Implied),
        );
    }

    #[test]
    fn decode_txa() {
        test_decode_imply(
            0x8A,
            Instruction::new(TXA, Implied),
        );
    }

    #[test]
    fn decode_tax() {
        test_decode_imply(
            0xAA,
            Instruction::new(TAX, Implied),
        );
    }

    #[test]
    fn decode_cpx() {
        test_decode_byte(
            0xE0, 0x44,
            Instruction::new(CPX, Immediate(0x44)),
        );
        test_decode_byte(
            0xE4, 0x44,
            Instruction::new(CPX, Zeropage(0x44)),
        );
        test_decode_word(
            0xEC, 0x4400,
            Instruction::new(CPX, Absolute(0x4400)),
        );
    }

    #[test]
    fn decode_cpy() {
        test_decode_byte(
            0xC0, 0x44,
            Instruction::new(CPY, Immediate(0x44)),
        );
        test_decode_byte(
            0xC4, 0x44,
            Instruction::new(CPY, Zeropage(0x44)),
        );
        test_decode_word(
            0xCC, 0x4400,
            Instruction::new(CPY, Absolute(0x4400)),
        );
    }

    #[test]
    fn decode_iny() {
        test_decode_imply(
            0xC8,
            Instruction::new(INY, Implied),
        );
    }

    #[test]
    fn decode_inx() {
        test_decode_imply(
            0xE8,
            Instruction::new(INX, Implied),
        );
    }

    #[test]
    fn decode_eor() {
        test_decode_byte(
            0x49, 0x44,
            Instruction::new(EOR, Immediate(0x44)),
        );
        test_decode_byte(
            0x45, 0x44,
            Instruction::new(EOR, Zeropage(0x44)),
        );
        test_decode_byte(
            0x55, 0x44,
            Instruction::new(EOR, ZeropageX(0x44)),
        );
        test_decode_word(
            0x4D, 0x4400,
            Instruction::new(EOR, Absolute(0x4400)),
        );
        test_decode_word(
            0x5D, 0x4400,
            Instruction::new(EOR, AbsoluteX(0x4400)),
        );
        test_decode_word(
            0x59, 0x4400,
            Instruction::new(EOR, AbsoluteY(0x4400)),
        );
        test_decode_byte(
            0x41, 0x44,
            Instruction::new(EOR, IndirectX(0x44)),
        );
        test_decode_byte(
            0x51, 0x44,
            Instruction::new(EOR, IndirectY(0x44)),
        );
    }

    #[test]
    fn decode_ora() {
        test_decode_byte(
            0x09, 0x44,
            Instruction::new(ORA, Immediate(0x44)),
        );
        test_decode_byte(
            0x05, 0x44,
            Instruction::new(ORA, Zeropage(0x44)),
        );
        test_decode_byte(
            0x15, 0x44,
            Instruction::new(ORA, ZeropageX(0x44)),
        );
        test_decode_word(
            0x0D, 0x4400,
            Instruction::new(ORA, Absolute(0x4400)),
        );
        test_decode_word(
            0x1D, 0x4400,
            Instruction::new(ORA, AbsoluteX(0x4400)),
        );
        test_decode_word(
            0x19, 0x4400,
            Instruction::new(ORA, AbsoluteY(0x4400)),
        );
        test_decode_byte(
            0x01, 0x44,
            Instruction::new(ORA, IndirectX(0x44)),
        );
        test_decode_byte(
            0x11, 0x44,
            Instruction::new(ORA, IndirectY(0x44)),
        );
    }

    #[test]
    fn decode_inc() {
        test_decode_byte(
            0xE6, 0x44,
            Instruction::new(INC, Zeropage(0x44)),
        );
        test_decode_byte(
            0xF6, 0x44,
            Instruction::new(INC, ZeropageX(0x44)),
        );
        test_decode_word(
            0xEE, 0x4400,
            Instruction::new(INC, Absolute(0x4400)),
        );
        test_decode_word(
            0xFE, 0x4400,
            Instruction::new(INC, AbsoluteX(0x4400)),
        );
    }

    #[test]
    fn decode_jsr() {
        test_decode_word(
            0x20, 0x4400,
            Instruction::new(JSR, Absolute(0x4400)),
        );
    }

    #[test]
    fn decode_dex() {
        test_decode_imply(
            0xCA,
            Instruction::new(DEX, Implied),
        );
    }

    #[test]
    fn decode_dey() {
        test_decode_imply(
            0x88,
            Instruction::new(DEY, Implied),
        );
    }

    #[test]
    fn decode_branch() {
        test_decode_byte(
            0x10, 0x44,
            Instruction::new(BPL, Relative(0x44)),
        );
        test_decode_byte(
            0x30, 0x44,
            Instruction::new(BMI, Relative(0x44)),
        );
        test_decode_byte(
            0x50, 0x44,
            Instruction::new(BVC, Relative(0x44)),
        );
        test_decode_byte(
            0x70, 0x44,
            Instruction::new(BVS, Relative(0x44)),
        );
        test_decode_byte(
            0x90, 0x44,
            Instruction::new(BCC, Relative(0x44)),
        );
        test_decode_byte(
            0xB0, 0x44,
            Instruction::new(BCS, Relative(0x44)),
        );
        test_decode_byte(
            0xD0, 0x44,
            Instruction::new(BNE, Relative(0x44)),
        );
        test_decode_byte(
            0xF0, 0x44,
            Instruction::new(BEQ, Relative(0x44)),
        );
    }

    #[test]
    fn decode_cmp() {
        test_decode_byte(
            0xC9, 0x44,
            Instruction::new(CMP, Immediate(0x44)),
        );
        test_decode_byte(
            0xC5, 0x44,
            Instruction::new(CMP, Zeropage(0x44)),
        );
        test_decode_byte(
            0xD5, 0x44,
            Instruction::new(CMP, ZeropageX(0x44)),
        );
        test_decode_word(
            0xCD, 0x4400,
            Instruction::new(CMP, Absolute(0x4400)),
        );
        test_decode_word(
            0xDD, 0x4400,
            Instruction::new(CMP, AbsoluteX(0x4400)),
        );
        test_decode_word(
            0xD9, 0x4400,
            Instruction::new(CMP, AbsoluteY(0x4400)),
        );
        test_decode_byte(
            0xC1, 0x44,
            Instruction::new(CMP, IndirectX(0x44)),
        );
        test_decode_byte(
            0xD1, 0x44,
            Instruction::new(CMP, IndirectY(0x44)),
        );
    }

    #[test]
    fn decode_dec() {
        test_decode_byte(
            0xC6, 0x44,
            Instruction::new(DEC, Zeropage(0x44)),
        );
        test_decode_byte(
            0xD6, 0x44,
            Instruction::new(DEC, ZeropageX(0x44)),
        );
        test_decode_word(
            0xCE, 0x4400,
            Instruction::new(DEC, Absolute(0x4400)),
        );
        test_decode_word(
            0xDE, 0x4400,
            Instruction::new(DEC, AbsoluteX(0x4400)),
        );
    }

    #[test]
    fn decode_brk() {
        test_decode_imply(
            0x00,
            Instruction::new(BRK, Implied),
        );
    }

    #[test]
    fn decode_ldy() {
        test_decode_byte(
            0xA0, 0x44,
            Instruction::new(LDY, Immediate(0x44)),
        );
        test_decode_byte(
            0xA4, 0x44,
            Instruction::new(LDY, Zeropage(0x44)),
        );
        test_decode_byte(
            0xB4, 0x44,
            Instruction::new(LDY, ZeropageX(0x44)),
        );
        test_decode_word(
            0xAC, 0x4400,
            Instruction::new(LDY, Absolute(0x4400)),
        );
        test_decode_word(
            0xBC, 0x4400,
            Instruction::new(LDY, AbsoluteX(0x4400)),
        );
    }

    #[test]
    fn decode_ldx() {
        test_decode_byte(
            0xA2, 0x44,
            Instruction::new(LDX, Immediate(0x44)),
        );
        test_decode_byte(
            0xA6, 0x44,
            Instruction::new(LDX, Zeropage(0x44)),
        );
        test_decode_byte(
            0xB6, 0x44,
            Instruction::new(LDX, ZeropageY(0x44)),
        );
        test_decode_word(
            0xAE, 0x4400,
            Instruction::new(LDX, Absolute(0x4400)),
        );
        test_decode_word(
            0xBE, 0x4400,
            Instruction::new(LDX, AbsoluteY(0x4400)),
        );
    }

    #[test]
    fn decode_sta() {
        test_decode_byte(
            0x85, 0x45,
            Instruction::new(STA, Zeropage(0x45)),
        );
        test_decode_byte(
            0x95, 0x45,
            Instruction::new(STA, ZeropageX(0x45)),
        );
        test_decode_word(
            0x8D, 0x1234,
            Instruction::new(STA, Absolute(0x1234)),
        );
        test_decode_word(
            0x9D, 0x1234,
            Instruction::new(STA, AbsoluteX(0x1234)),
        );
        test_decode_word(
            0x99, 0x1234,
            Instruction::new(STA, AbsoluteY(0x1234)),
        );
        test_decode_byte(
            0x81, 0x12,
            Instruction::new(STA, IndirectX(0x12)),
        );
        test_decode_byte(
            0x91, 0x12,
            Instruction::new(STA, IndirectY(0x12)),
        );
    }

    #[test]
    fn decode_lda() {
        test_decode_byte(
            0xA9, 0x45,
            Instruction::new(LDA, Immediate(0x45)),
        );
        test_decode_byte(
            0xA5, 0x45,
            Instruction::new(LDA, Zeropage(0x45)),
        );
        test_decode_byte(
            0xB5, 0x45,
            Instruction::new(LDA, ZeropageX(0x45)),
        );
        test_decode_word(
            0xAD, 0x1234,
            Instruction::new(LDA, Absolute(0x1234)),
        );
        test_decode_word(
            0xBD, 0x1234,
            Instruction::new(LDA, AbsoluteX(0x1234)),
        );
        test_decode_word(
            0xB9, 0x1234,
            Instruction::new(LDA, AbsoluteY(0x1234)),
        );
        test_decode_byte(
            0xA1, 0x12,
            Instruction::new(LDA, IndirectX(0x12)),
        );
        test_decode_byte(
            0xB1, 0x12,
            Instruction::new(LDA, IndirectY(0x12)),
        );
    }

    #[test]
    fn decode_sec() {
        test_decode_imply(
            0x38,
            Instruction::new(SEC, Implied),
        );
    }

    #[test]
    fn decode_sed() {
        test_decode_imply(
            0xF8,
            Instruction::new(SED, Implied),
        );
    }

    #[test]
    fn decode_sei() {
        test_decode_imply(
            0x78,
            Instruction::new(SEI, Implied),
        );
    }

    #[test]
    fn decode_php() {
        test_decode_imply(
            0x08,
            Instruction::new(PHP, Implied),
        );
    }

    #[test]
    fn decode_plp() {
        test_decode_imply(
            0x28,
            Instruction::new(PLP, Implied),
        );
    }

    #[test]
    fn decode_pla() {
        test_decode_imply(
            0x68,
            Instruction::new(PLA, Implied),
        );
    }

    #[test]
    fn decode_pha() {
        test_decode_imply(
            0x48,
            Instruction::new(PHA, Implied),
        );
    }

    #[test]
    fn decode_tsx() {
        test_decode_imply(
            0xBA,
            Instruction::new(TSX, Implied),
        );
    }

    #[test]
    fn decode_txs() {
        test_decode_imply(
            0x9A,
            Instruction::new(TXS, Implied),
        );
    }

    #[test]
    fn decode_jmp_absolute() {
        test_decode_word(
            0x4C, 0x1234,
            Instruction::new(JMP, Absolute(0x1234)),
        );
    }

    #[test]
    fn decode_jmp_indirect() {
        test_decode_word(
            0x6C, 0x1234,
            Instruction::new(JMP, Indirect(0x1234)),
        );
    }

    #[test]
    fn decode_nop() {
        test_decode_imply(
            0xEA,
            Instruction::new(NOP, Implied),
        );
    }

    #[test]
    fn decode_asl_acc() {
        test_decode_imply(
            0x0A,
            Instruction::new(ASL, Accumulator),
        );
    }

    #[test]
    fn decode_asl_zeropage() {
        test_decode_byte(
            0x06, 0x44,
            Instruction::new(ASL, Zeropage(0x44)),
        );
    }

    #[test]
    fn decode_asl_zeropage_x() {
        test_decode_byte(
            0x16, 0x44,
            Instruction::new(ASL, ZeropageX(0x44)),
        );
    }

    #[test]
    fn decode_asl_absolute() {
        test_decode_word(
            0x0E, 0x1234,
            Instruction::new(ASL, Absolute(0x1234)),
        );
    }

    #[test]
    fn decode_asl_absolute_x() {
        test_decode_word(
            0x1E, 0x1234,
            Instruction::new(ASL, AbsoluteX(0x1234)),
        );
    }

    #[test]
    fn decode_lsr_acc() {
        test_decode_imply(
            0x4A,
            Instruction::new(LSR, Accumulator),
        );
    }

    #[test]
    fn decode_lsr_zeropage() {
        test_decode_byte(
            0x46, 0x44,
            Instruction::new(LSR, Zeropage(0x44)),
        );
    }

    #[test]
    fn decode_lsr_zeropage_x() {
        test_decode_byte(
            0x56, 0x44,
            Instruction::new(LSR, ZeropageX(0x44)),
        );
    }

    #[test]
    fn decode_lsr_absolute() {
        test_decode_word(
            0x4E, 0x1234,
            Instruction::new(LSR, Absolute(0x1234)),
        );
    }

    #[test]
    fn decode_lsr_absolute_x() {
        test_decode_word(
            0x5E, 0x1234,
            Instruction::new(LSR, AbsoluteX(0x1234)),
        );
    }

    #[test]
    fn decode_clc() {
        test_decode_imply(
            0x18,
            Instruction::new(CLC, Implied),
        );
    }

    #[test]
    fn decode_cld() {
        test_decode_imply(
            0xD8,
            Instruction::new(CLD, Implied),
        );
    }

    #[test]
    fn decode_cli() {
        test_decode_imply(
            0x58,
            Instruction::new(CLI, Implied),
        );
    }

    #[test]
    fn decode_clv() {
        test_decode_imply(
            0xB8,
            Instruction::new(CLV, Implied),
        );
    }
}
