use crate::bus::Bus;
use crate::cpu::CPU;
use crate::cpu::instructions::{AddressMode, Instruction, Mnemonic};
use crate::cpu::status_register::Flag;

fn check_page_crossing(left: u16, right: u16) -> bool {
    (left & 0xFF00) != (right & 0xFF00)
}

impl CPU {
    /// Execute the given instruction on the CPU.
    pub fn execute<B: Bus>(&mut self, bus: &mut B, instr: &Instruction) -> u8 {
        use Mnemonic::*;

        match instr.mnemonic {
            JMP => self.execute_jmp(bus, &instr.mode),
            STY => self.execute_sty(bus, &instr.mode),
            STX => self.execute_stx(bus, &instr.mode),
            NOP => self.execute_nop(bus, &instr.mode),
            ASL => self.execute_asl(bus, &instr.mode),
            LSR => self.execute_lsr(bus, &instr.mode),
            TXS => self.execute_txs(bus, &instr.mode),
            TSX => self.execute_tsx(bus, &instr.mode),
            PHA => self.execute_pha(bus, &instr.mode),
            PLA => self.execute_pla(bus, &instr.mode),
            PHP => self.execute_php(bus, &instr.mode),
            PLP => self.execute_plp(bus, &instr.mode),
            LDA => self.execute_lda(bus, &instr.mode),
            STA => self.execute_sta(bus, &instr.mode),
            BRK => self.execute_brk(bus, &instr.mode),
            LDX => self.execute_ldx(bus, &instr.mode),
            LDY => self.execute_ldy(bus, &instr.mode),
            BPL => self.execute_branch(bus, &instr.mode, !self.flag(Flag::N)),
            BMI => self.execute_branch(bus, &instr.mode, self.flag(Flag::N)),
            BVC => self.execute_branch(bus, &instr.mode, !self.flag(Flag::V)),
            BVS => self.execute_branch(bus, &instr.mode, self.flag(Flag::V)),
            BCC => self.execute_branch(bus, &instr.mode, !self.flag(Flag::C)),
            BCS => self.execute_branch(bus, &instr.mode, self.flag(Flag::C)),
            BNE => self.execute_branch(bus, &instr.mode, !self.flag(Flag::Z)),
            BEQ => self.execute_branch(bus, &instr.mode, self.flag(Flag::Z)),
            DEC => self.execute_dec(bus, &instr.mode),
            DEX => self.execute_dex(bus, &instr.mode),
            DEY => self.execute_dey(bus, &instr.mode),
            INX => self.execute_inx(bus, &instr.mode),
            INY => self.execute_iny(bus, &instr.mode),
            INC => self.execute_inc(bus, &instr.mode),
            CMP => self.execute_cmp(bus, &instr.mode),
            ORA => self.execute_ora(bus, &instr.mode),
            EOR => self.execute_eor(bus, &instr.mode),
            JSR => self.execute_jsr(bus, &instr.mode),
            CPX => self.execute_cpx(bus, &instr.mode),
            CPY => self.execute_cpy(bus, &instr.mode),
            TAX => self.execute_tax(bus, &instr.mode),
            TAY => self.execute_tay(bus, &instr.mode),
            TXA => self.execute_txa(bus, &instr.mode),
            TYA => self.execute_tya(bus, &instr.mode),
            ROL => self.execute_rol(bus, &instr.mode),
            ROR => self.execute_ror(bus, &instr.mode),
            CLC => self.execute_clear(bus, &instr.mode, Flag::C),
            CLD => self.execute_clear(bus, &instr.mode, Flag::D),
            CLI => self.execute_clear(bus, &instr.mode, Flag::I),
            CLV => self.execute_clear(bus, &instr.mode, Flag::V),
            SEC => self.execute_set(bus, &instr.mode, Flag::C),
            SED => self.execute_set(bus, &instr.mode, Flag::D),
            SEI => self.execute_set(bus, &instr.mode, Flag::I),
            XXX => panic!("XXX instruction is emulator reserved."),
            _ => { unimplemented!() }
        }
    }

    pub fn interrupt<B: Bus>(&mut self, bus: &mut B, addr: u16) {
        self.save_pc(bus);
        self.stack_push(bus, self.sr);

        self.set_flag(Flag::I, true);
        self.pc = bus.read_word(addr);

        self.remaining_cycles += 6;
        self.total_cycles += 1;
    }

    /// Return the effective address from the address mode and also return a
    /// boolean indicating a page boundary crossing.
    fn compute_word_operand<B: Bus>(&self, bus: &mut B, mode: &AddressMode) -> (u16, bool) {
        use AddressMode::*;

        match *mode {
            Absolute(val) => (val, false),
            AbsoluteX(val) => {
                let res: u16 = val + (self.x as u16);
                (res, check_page_crossing(val, res))
            }
            AbsoluteY(val) => {
                let res: u16 = val + (self.y as u16);
                (res, check_page_crossing(val, res))
            }
            Indirect(val) => (bus.read_word(val), false),
            IndirectX(val) => (bus.read_word((val + self.x) as u16 % 256), false),
            IndirectY(val) => {
                let addr: u16 = bus.read_word(val as u16);
                let res = addr + (self.y as u16);
                (res, check_page_crossing(addr, res))
            }
            Zeropage(val) => (val as u16, false),
            ZeropageX(val) => ((val + self.x) as u16 % 256, false),
            ZeropageY(val) => ((val + self.y) as u16 % 256, false),
            Relative(val) => {
                let val = (val as i16) - 256;
                let res = (self.pc as i32) + (val as i32);
                (res as u16, false)
            }
            _ => panic!("Incompatible address mode.")
        }
    }

    /// Fetch byte from bus with the effective address. Also return a
    /// boolean indicating page boundary crossing.
    fn fetch_byte<B: Bus>(&self, bus: &mut B, mode: &AddressMode) -> (u8, bool) {
        use AddressMode::*;

        match *mode {
            Accumulator => (self.ac, false),
            Immediate(val) => (val, false),
            Zeropage(_) | ZeropageX(_) | ZeropageY(_) |
            Absolute(_) | AbsoluteX(_) => {
                let (addr, crossing) = self.compute_word_operand(bus, mode);
                (bus.read_byte(addr), crossing)
            }
            _ => panic!("Incompatible address mode."),
        }
    }

    /// Set byte in bus with the effective address. Also return a boolean
    /// indicating page boundary crossing.
    fn set_byte<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode, byte: u8) -> bool {
        use AddressMode::*;

        match *mode {
            Accumulator => {
                self.ac = byte;
                false
            }
            Zeropage(_) | ZeropageX(_) | ZeropageY(_) |
            Absolute(_) | AbsoluteX(_) => {
                let (addr, crossing) = self.compute_word_operand(bus, mode);
                bus.write_byte(addr, byte);
                crossing
            }
            _ => panic!("Incompatible address mode."),
        }
    }

    fn save_pc<B: Bus>(&mut self, bus: &mut B) {
        let lo = self.pc & 0x00FF;
        let hi = (self.pc & 0xFF00) >> 8;

        self.stack_push(bus, lo as u8);
        self.stack_push(bus, hi as u8);
    }

    /// Push a value on the stack.
    fn stack_push<B: Bus>(&mut self, bus: &mut B, byte: u8) {
        bus.write_byte(0x100 + self.sp as u16, byte);
        self.sp = self.sp.wrapping_sub(1);
    }

    /// Pull a value from the stack.
    fn stack_pull<B: Bus>(&mut self, bus: &mut B) -> u8 {
        self.sp = self.sp.wrapping_add(1);
        bus.read_byte(0x100 + self.sp as u16)
    }

    /// Update flags based on last result.
    fn update_flags(&mut self, value: u8) {
        self.set_flag(Flag::Z, value == 0);
        self.set_flag(Flag::N, (value & 0b1000_0000) > 0);
    }

    fn execute_tay<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.y = self.ac;

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_tya<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.ac = self.y;

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_tax<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.x = self.ac;

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_txa<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.ac = self.x;

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_eor<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (value, crossing) = self.fetch_byte(bus, mode);
        self.ac ^= value;
        self.update_flags(self.ac);

        match *mode {
            Immediate(_) => 2,
            Zeropage(_) => 3,
            ZeropageX(_) => 4,
            Absolute(_) => 4,
            AbsoluteX(_) => 4 + (crossing as u8),
            AbsoluteY(_) => 4 + (crossing as u8),
            IndirectX(_) => 6,
            IndirectY(_) => 5 + (crossing as u8),
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_ora<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (value, crossing) = self.fetch_byte(bus, mode);
        self.ac |= value;
        self.update_flags(self.ac);

        match *mode {
            Immediate(_) => 2,
            Zeropage(_) => 3,
            ZeropageX(_) => 4,
            Absolute(_) => 4,
            AbsoluteX(_) => 4 + (crossing as u8),
            AbsoluteY(_) => 4 + (crossing as u8),
            IndirectX(_) => 6,
            IndirectY(_) => 5 + (crossing as u8),
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_inc<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (mut value, _) = self.fetch_byte(bus, mode);
        value = value.wrapping_add(1);
        self.set_byte(bus, mode, value);
        self.update_flags(value);

        match *mode {
            Zeropage(_) => 5,
            ZeropageX(_) => 6,
            Absolute(_) => 6,
            AbsoluteX(_) => 7,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_jsr<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.save_pc(bus);
        let (result, _) = self.compute_word_operand(bus, mode);
        self.pc = result;

        match *mode {
            Absolute(_) => 6,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_iny<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.y = self.y.wrapping_add(1);

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_inx<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.x = self.x.wrapping_add(1);

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_dey<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.y = self.y.wrapping_sub(1);

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_dex<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.x = self.x.wrapping_sub(1);

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode"),
        }
    }

    fn compare<B: Bus>(&mut self, bus: &mut B,
                       mode: &AddressMode, byte: u8) -> bool {
        let (value, crossing) = self.fetch_byte(bus, mode);
        let sub = byte.wrapping_sub(value);

        self.set_flag(Flag::C, byte >= value);
        self.set_flag(Flag::Z, byte == value);
        self.set_flag(Flag::N, sub & 0b1000_0000 > 0);

        crossing
    }

    fn execute_cpx<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.compare(bus, mode, self.x);

        match *mode {
            Immediate(_) => 2,
            Zeropage(_) => 3,
            Absolute(_) => 4,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_cpy<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.compare(bus, mode, self.y);

        match *mode {
            Immediate(_) => 2,
            Zeropage(_) => 3,
            Absolute(_) => 4,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_cmp<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let crossing = self.compare(bus, mode, self.ac);

        match *mode {
            Immediate(_) => 2,
            Zeropage(_) => 3,
            ZeropageX(_) => 4,
            Absolute(_) => 4,
            AbsoluteX(_) => 4 + crossing as u8,
            AbsoluteY(_) => 4 + crossing as u8,
            IndirectX(_) => 6,
            IndirectY(_) => 5 + crossing as u8,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_dec<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (mut value, _) = self.fetch_byte(bus, mode);
        value = value.wrapping_sub(1);
        self.set_byte(bus, mode, value);
        self.update_flags(value);

        match *mode {
            Zeropage(_) => 5,
            ZeropageX(_) => 6,
            Absolute(_) => 6,
            AbsoluteX(_) => 7,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_branch<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode, cond: bool) -> u8 {
        use AddressMode::*;

        let mut overhead = 0;

        if cond {
            let (result, _) = self.compute_word_operand(bus, mode);

            if check_page_crossing(self.pc, result) {
                overhead += 2;
            } else {
                overhead += 1;
            }

            self.pc = result;
        }

        match *mode {
            Relative(_) => 2 + overhead,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_ldy<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (result, crossing) = self.fetch_byte(bus, mode);
        self.y = result;
        self.update_flags(self.y);

        match *mode {
            Immediate(_) => 2,
            Zeropage(_) => 3,
            ZeropageY(_) => 4,
            Absolute(_) => 4,
            AbsoluteY(_) => 4 + (crossing as u8),
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_ldx<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (result, crossing) = self.fetch_byte(bus, mode);
        self.x = result;
        self.update_flags(self.x);

        match *mode {
            Immediate(_) => 2,
            Zeropage(_) => 3,
            ZeropageY(_) => 4,
            Absolute(_) => 4,
            AbsoluteY(_) => 4 + (crossing as u8),
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_brk<B: Bus>(&mut self, bus: &mut B, _mode: &AddressMode) -> u8 {
        self.irq(bus);
        0
    }

    fn execute_sta<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.set_byte(bus, mode, self.ac);

        match *mode {
            Zeropage(_) => 3,
            ZeropageX(_) => 4,
            Absolute(_) => 4,
            AbsoluteX(_) => 5,
            AbsoluteY(_) => 5,
            IndirectX(_) => 6,
            IndirectY(_) => 6,
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_lda<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (byte, crossing) = self.fetch_byte(bus, mode);
        self.ac = byte;
        self.update_flags(self.ac);

        match *mode {
            Immediate(_) => 2,
            Zeropage(_) => 3,
            ZeropageX(_) => 4,
            Absolute(_) => 4,
            AbsoluteX(_) => 4 + (crossing as u8),
            AbsoluteY(_) => 4 + (crossing as u8),
            IndirectX(_) => 5,
            IndirectY(_) => 6 + (crossing as u8),
            _ => panic!("Illegal address mode"),
        }
    }

    fn execute_plp<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.sr = self.stack_pull(bus);

        match mode {
            Implied => 4,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_php<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.stack_push(bus, self.sr);

        match *mode {
            Implied => 3,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_pla<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.ac = self.stack_pull(bus);

        match *mode {
            Implied => 4,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_pha<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.stack_push(bus, self.ac);

        match *mode {
            Implied => 3,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_tsx<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.x = self.sp;

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_txs<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.sp = self.x;

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_set<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode, flag: Flag) -> u8 {
        use AddressMode::*;

        self.set_flag(flag, true);

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_clear<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode, flag: Flag) -> u8 {
        use AddressMode::*;

        self.set_flag(flag, false);

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode."),
        }
    }

    fn execute_jmp<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (addr, _) = self.compute_word_operand(bus, mode);
        self.pc = addr;

        match *mode {
            Absolute(_) => 3,
            Indirect(_) => 5,
            _ => panic!("Illegal address mode.")
        }
    }

    fn execute_sty<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.set_byte(bus, mode, self.y);

        match *mode {
            Zeropage(_) => 3,
            ZeropageX(_) => 4,
            Absolute(_) => 4,
            _ => panic!("Illegal address mode.")
        }
    }

    fn execute_stx<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        self.set_byte(bus, mode, self.x);

        match *mode {
            Zeropage(_) => 3,
            ZeropageY(_) => 4,
            Absolute(_) => 4,
            _ => { panic!("Illegal address mode.") }
        }
    }

    fn execute_nop<B: Bus>(&mut self, _bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        match *mode {
            Implied => 2,
            _ => panic!("Illegal address mode.")
        }
    }

    fn execute_asl<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (mut value, _) = self.fetch_byte(bus, mode);

        self.set_flag(Flag::C, (value & 0b1000_0000) > 0);
        value <<= 1;
        self.update_flags(value);

        self.set_byte(bus, mode, value);

        match *mode {
            Accumulator => 2,
            Zeropage(_) => 5,
            ZeropageX(_) => 6,
            Absolute(_) => 6,
            AbsoluteX(_) => 7,
            _ => panic!("Illegal address mode.")
        }
    }

    fn execute_ror<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (mut value, _) = self.fetch_byte(bus, mode);

        let carry = self.flag(Flag::C) as u8;
        self.set_flag(Flag::C, (value & 0b0000_0001) > 0);
        value = (value >> 1) | (carry << 7);
        self.update_flags(value);

        self.set_byte(bus, mode, value);

        match *mode {
            Accumulator => 2,
            Zeropage(_) => 5,
            ZeropageX(_) => 6,
            Absolute(_) => 6,
            AbsoluteX(_) => 7,
            _ => panic!("Illegal address mode")
        }
    }

    fn execute_rol<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (mut value, _) = self.fetch_byte(bus, mode);

        let carry = self.flag(Flag::C) as u8;
        self.set_flag(Flag::C, (value & 0b1000_0000) > 0);
        value = (value << 1) | carry;
        self.update_flags(value);

        self.set_byte(bus, mode, value);

        match *mode {
            Accumulator => 2,
            Zeropage(_) => 5,
            ZeropageX(_) => 6,
            Absolute(_) => 6,
            AbsoluteX(_) => 7,
            _ => panic!("Illegal address mode")
        }
    }

    fn execute_lsr<B: Bus>(&mut self, bus: &mut B, mode: &AddressMode) -> u8 {
        use AddressMode::*;

        let (mut value, _) = self.fetch_byte(bus, mode);

        self.set_flag(Flag::C, (value & 0b0000_0001) > 0);
        value >>= 1;
        self.update_flags(value);

        self.set_byte(bus, mode, value);

        match *mode {
            Accumulator => 2,
            Zeropage(_) => 5,
            ZeropageX(_) => 6,
            Absolute(_) => 6,
            AbsoluteX(_) => 7,
            _ => panic!("Illegal address mode.")
        }
    }
}

#[cfg(test)]
mod test {
    use crate::cpu::mocking::*;

    use super::*;

    #[test]
    fn ror() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ROR,
            mode: AddressMode::Accumulator,
        };

        cpu.set_flag(Flag::C, false);
        cpu.ac = 0b1000_0001;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.ac, 0b0100_0000);
        assert_eq!(cpu.flag(Flag::C), true);
    }

    #[test]
    fn rol() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ROL,
            mode: AddressMode::Accumulator,
        };

        cpu.set_flag(Flag::C, true);
        cpu.ac = 0b0100_0000;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.ac, 0b1000_0001);
        assert_eq!(cpu.flag(Flag::C), false);
    }

    #[test]
    fn tya() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::TYA,
            mode: AddressMode::Implied,
        };

        cpu.y = 0x23;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.ac, 0x23);
    }

    #[test]
    fn tay() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::TAY,
            mode: AddressMode::Implied,
        };

        cpu.ac = 0x23;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.y, 0x23);
    }

    #[test]
    fn txa() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::TXA,
            mode: AddressMode::Implied,
        };

        cpu.x = 0x23;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.ac, 0x23);
    }

    #[test]
    fn tax() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::TAX,
            mode: AddressMode::Implied,
        };

        cpu.ac = 0x23;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.x, 0x23);
    }

    #[test]
    fn cpy() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CPY,
            mode: AddressMode::Immediate(0x44),
        };

        cpu.y = 0x44;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.flag(Flag::Z), true);
        assert_eq!(cpu.flag(Flag::C), true);
        assert_eq!(cpu.flag(Flag::N), false);
    }

    #[test]
    fn cpx() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CPX,
            mode: AddressMode::Immediate(0x44),
        };

        cpu.x = 0x44;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.flag(Flag::Z), true);
        assert_eq!(cpu.flag(Flag::C), true);
        assert_eq!(cpu.flag(Flag::N), false);
    }

    #[test]
    fn eor() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::EOR,
            mode: AddressMode::Immediate(0x44),
        };

        cpu.ac = 0x12;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.ac, 0x12 ^ 0x44);
    }

    #[test]
    fn ora() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ORA,
            mode: AddressMode::Immediate(0x44),
        };

        cpu.ac = 0x12;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.ac, 0x12 | 0x44);
    }

    #[test]
    fn inc() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::INC,
            mode: AddressMode::Absolute(0x1234),
        };

        bus_mock.read_byte
            .given(Matcher::Val(0x1234))
            .will_return(0x34);

        assert_eq!(cpu.execute(&mut bus_mock, &instr), 6);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x1234, 0x35))
        ));
    }

    #[test]
    fn jsr() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::JSR,
            mode: AddressMode::Absolute(0x1234),
        };

        cpu.pc = 0x4400;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 6);
        assert!(
            verify(
                bus_mock.write_byte
                    .was_called_with((0x1FF, 0x00))
            )
        );
        assert!(
            verify(
                bus_mock.write_byte
                    .was_called_with((0x1FE, 0x44))
            )
        );
        assert_eq!(cpu.pc, 0x1234);
    }

    #[test]
    fn cmp_eq() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CMP,
            mode: AddressMode::Absolute(0x1234),
        };

        cpu.ac = 0x12;
        bus_mock.read_byte
            .given(Matcher::Val(0x1234))
            .will_return(0x12);

        assert_eq!(cpu.execute(&mut bus_mock, &instr), 4);
        assert_eq!(cpu.flag(Flag::Z), true);
        assert_eq!(cpu.flag(Flag::C), true);
        assert_eq!(cpu.flag(Flag::N), false);
    }

    #[test]
    fn cmp_great() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CMP,
            mode: AddressMode::Absolute(0x1234),
        };

        cpu.ac = 0x34;
        bus_mock.read_byte
            .given(Matcher::Val(0x1234))
            .will_return(0x12);

        assert_eq!(cpu.execute(&mut bus_mock, &instr), 4);
        assert_eq!(cpu.flag(Flag::Z), false);
        assert_eq!(cpu.flag(Flag::C), true);
        assert_eq!(cpu.flag(Flag::N), false);
    }

    #[test]
    fn cmp_less() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CMP,
            mode: AddressMode::Absolute(0x1234),
        };

        cpu.ac = 0x12;
        bus_mock.read_byte
            .given(Matcher::Val(0x1234))
            .will_return(0x34);

        assert_eq!(cpu.execute(&mut bus_mock, &instr), 4);
        assert_eq!(cpu.flag(Flag::Z), false);
        assert_eq!(cpu.flag(Flag::C), false);
        assert_eq!(cpu.flag(Flag::N), true);
    }

    #[test]
    fn branch() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let mode = AddressMode::Relative((256 - 4) as u8);

        cpu.pc = 0x4;
        assert_eq!(cpu.execute_branch(&mut bus_mock, &mode, true), 3);
        assert_eq!(cpu.pc, 0x0);
    }

    #[test]
    fn bpl_n_clear() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::BPL,
            mode: AddressMode::Relative((256 - 4) as u8),
        };
        cpu.set_flag(Flag::N, false);
        cpu.pc = 0x4;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 3);
        assert_eq!(cpu.pc, 0x0);
    }

    #[test]
    fn bpl_n_set() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::BPL,
            mode: AddressMode::Relative(0x92),
        };
        cpu.set_flag(Flag::N, true);
        cpu.pc = 0x50;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.pc, 0x50);
    }

    #[test]
    fn iny() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::INY,
            mode: AddressMode::Implied,
        };

        cpu.y = 0x34;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.y, 0x35);
    }

    #[test]
    fn inx() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::INX,
            mode: AddressMode::Implied,
        };

        cpu.x = 0xFF;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.x, 0x00);
    }


    #[test]
    fn dey() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::DEY,
            mode: AddressMode::Implied,
        };

        cpu.y = 0x34;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.y, 0x33);
    }

    #[test]
    fn dex() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::DEX,
            mode: AddressMode::Implied,
        };

        cpu.x = 0x00;
        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.x, 0xFF);
    }

    #[test]
    fn dec() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::DEC,
            mode: AddressMode::Absolute(0x1234),
        };

        bus_mock.read_byte
            .given(Matcher::Val(0x1234))
            .will_return(0x34);

        assert_eq!(cpu.execute(&mut bus_mock, &instr), 6);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x1234, 0x33))
        ));
    }

    #[test]
    fn ldx() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::LDX,
            mode: AddressMode::Immediate(0x44),
        };

        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.x, 0x44);
    }

    #[test]
    fn ldy() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::LDY,
            mode: AddressMode::Immediate(0x44),
        };

        assert_eq!(cpu.execute(&mut bus_mock, &instr), 2);
        assert_eq!(cpu.y, 0x44);
    }

    #[test]
    fn brk() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::BRK,
            mode: AddressMode::Implied,
        };

        bus_mock.read_word
            .given(Matcher::Val(0xFFFE))
            .will_return(0x1234);

        assert_eq!(cpu.execute(&mut bus_mock, &instr), 0);
        assert_eq!(cpu.pc, 0x1234);
    }

    #[test]
    fn interrupt() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.pc = 0x4556;
        bus_mock.read_word
            .given(Matcher::Val(0xFFFE))
            .will_return(0x1234);

        cpu.interrupt(&mut bus_mock, 0xFFFE);
        assert_eq!(cpu.pc, 0x1234);
        assert!(
            verify(
                bus_mock.write_byte
                    .was_called_with((0x01FF, 0x56))
            )
        );
        assert!(
            verify(
                bus_mock.write_byte
                    .was_called_with((0x01FE, 0x45))
            )
        );
    }

    #[test]
    fn sta() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        let instr = Instruction {
            mnemonic: Mnemonic::STA,
            mode: AddressMode::Absolute(0x1234),
        };

        cpu.ac = 0x10;
        let cycles = cpu.execute(&mut bus_mock, &instr);
        assert_eq!(cycles, 4);
        assert!(
            verify(
                bus_mock.write_byte
                    .was_called_with((0x1234, 0x10))
            )
        );
    }

    #[test]
    fn lda() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        bus_mock.read_byte
            .given(Matcher::Val(0x1234))
            .will_return(0x23);

        let instr = Instruction {
            mnemonic: Mnemonic::LDA,
            mode: AddressMode::Absolute(0x1234),
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);
        assert_eq!(cycles, 4);
        assert_eq!(cpu.ac, 0x23);
    }

    #[test]
    fn zeropage() {
        let mut bus_mock = BusMock::new();
        let cpu = CPU::new();

        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::Zeropage(0x32)),
            (0x0032, false)
        );
    }

    #[test]
    fn zeropage_x() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.x = 3;
        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::ZeropageX(0x32)),
            (0x0035, false),
        );
    }

    #[test]
    fn zeropage_y() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.y = 5;
        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::ZeropageY(0x32)),
            (0x0037, false)
        );
    }

    #[test]
    fn absolute() {
        let mut bus_mock = BusMock::new();
        let cpu = CPU::new();

        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::Absolute(0x1234)),
            (0x1234, false),
        );
    }

    #[test]
    fn absolute_x() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.x = 3;
        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::AbsoluteX(0x12FF)),
            (0x1302, true)
        );
    }

    #[test]
    fn absolute_y() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.y = 6;
        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::AbsoluteY(0x12FF)),
            (0x1305, true)
        );
    }

    #[test]
    fn indirect() {
        let mut bus_mock = BusMock::new();
        let cpu = CPU::new();

        bus_mock.read_word
            .given(Matcher::Val(0x1234))
            .will_return(0x5678);

        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::Indirect(0x1234)),
            (0x5678, false)
        );
    }

    #[test]
    fn indexed_indirect() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.x = 4;
        bus_mock.read_word
            .given(Matcher::Val(0x0034))
            .will_return(0x5678);

        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::IndirectX(0x30)),
            (0x5678, false),
        );
    }

    #[test]
    fn indirect_indexed() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.y = 3;
        bus_mock.read_word
            .given(Matcher::Val(0x0034))
            .will_return(0x5675);

        assert_eq!(
            cpu.compute_word_operand(&mut bus_mock, &AddressMode::IndirectY(0x34)),
            (0x5678, false)
        );
    }

    #[test]
    fn stack_push() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.sp = 0x57;
        cpu.stack_push(&mut bus_mock, 42);
        assert_eq!(cpu.sp, 0x56);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0157, 42))
        ));
    }

    #[test]
    fn stack_push_wrapping() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.sp = 0x00;
        cpu.stack_push(&mut bus_mock, 42);
        assert_eq!(cpu.sp, 0xFF);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0100, 42))
        ));
    }

    #[test]
    fn stack_pull() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.sp = 0x57;
        bus_mock.read_byte
            .given(Matcher::Val(0x158))
            .will_return(42);

        assert_eq!(cpu.stack_pull(&mut bus_mock), 42);
        assert_eq!(cpu.sp, 0x58);
    }

    #[test]
    fn stack_pull_wrapping() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();

        cpu.sp = 0xFF;
        bus_mock.read_byte
            .given(Matcher::Val(0x100))
            .will_return(42);

        assert_eq!(cpu.stack_pull(&mut bus_mock), 42);
        assert_eq!(cpu.sp, 0x00);
    }

    #[test]
    fn plp() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::PLP,
            mode: AddressMode::Implied,
        };

        cpu.sp = 0xFE;
        bus_mock.read_byte
            .given(Matcher::Val(0x1FF))
            .will_return(0x56);

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 4);
        assert_eq!(cpu.sr, 0x56);
        assert_eq!(cpu.sp, 0xFF);
    }

    #[test]
    fn php() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::PHP,
            mode: AddressMode::Implied,
        };

        cpu.sr = 0x45;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 3);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x01FF, 0x45))
        ));
        assert_eq!(cpu.sp, 0xFE);
    }


    #[test]
    fn pla() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::PLA,
            mode: AddressMode::Implied,
        };

        cpu.sp = 0xFE;
        bus_mock.read_byte
            .given(Matcher::Val(0x1FF))
            .will_return(0x56);

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 4);
        assert_eq!(cpu.ac, 0x56);
        assert_eq!(cpu.sp, 0xFF);
    }

    #[test]
    fn pha() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::PHA,
            mode: AddressMode::Implied,
        };

        cpu.ac = 0x45;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 3);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x01FF, 0x45))
        ));
        assert_eq!(cpu.sp, 0xFE);
    }

    #[test]
    fn tsx() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::TSX,
            mode: AddressMode::Implied,
        };

        cpu.sp = 0x23;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.x, 0x23);
    }

    #[test]
    fn txs() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::TXS,
            mode: AddressMode::Implied,
        };

        cpu.x = 0x12;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.sp, 0x12);
    }

    #[test]
    fn jmp_absolute() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::JMP,
            mode: AddressMode::Absolute(0x5597),
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 3);
        assert_eq!(cpu.pc, 0x5597);
    }

    #[test]
    fn jmp_indirect() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::JMP,
            mode: AddressMode::Indirect(0x5597),
        };

        bus_mock.read_word
            .given(Matcher::Val(0x5597))
            .will_return(0x1234);

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 5);
        assert_eq!(cpu.pc, 0x1234);
    }

    #[test]
    fn sty_zeropage() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        cpu.y = 0x12;

        let instr = Instruction {
            mnemonic: Mnemonic::STY,
            mode: AddressMode::Zeropage(0x13),
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 3);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0013, 0x12))
        ));
    }

    #[test]
    fn sty_zeropage_x() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        cpu.y = 0x16;
        cpu.x = 0x03;

        let instr = Instruction {
            mnemonic: Mnemonic::STY,
            mode: AddressMode::ZeropageX(0x20),
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 4);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0023, 0x16))
        ));
    }

    #[test]
    fn sty_absolute() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        cpu.y = 0x45;

        let instr = Instruction {
            mnemonic: Mnemonic::STY,
            mode: AddressMode::Absolute(0x4321),
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 4);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x4321, 0x45))
        ));
    }

    #[test]
    fn stx_zeropage() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        cpu.x = 0x12;

        let instr = Instruction {
            mnemonic: Mnemonic::STX,
            mode: AddressMode::Zeropage(0x13),
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 3);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0013, 0x12))
        ));
    }

    #[test]
    fn stx_zeropage_x() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        cpu.x = 0x34;
        cpu.y = 0x06;

        let instr = Instruction {
            mnemonic: Mnemonic::STX,
            mode: AddressMode::ZeropageY(0x32),
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 4);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0038, 0x34))
        ));
    }

    #[test]
    fn stx_absolute() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        cpu.x = 0x21;

        let instr = Instruction {
            mnemonic: Mnemonic::STX,
            mode: AddressMode::Absolute(0x2315),
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 4);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x2315, 0x21))
        ));
    }

    #[test]
    fn nop() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::NOP,
            mode: AddressMode::Implied,
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
    }

    #[test]
    fn asl_accumulator() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ASL,
            mode: AddressMode::Accumulator,
        };

        cpu.ac = 0xAF;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.ac, 0x5E);
    }

    #[test]
    fn asl_accumulator_flags() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ASL,
            mode: AddressMode::Accumulator,
        };

        cpu.ac = 0b0100_0000;
        cpu.sr = 0x0;
        cpu.execute(&mut bus_mock, &instr);
        assert_eq!(cpu.flag(Flag::C), false);
        assert_eq!(cpu.flag(Flag::Z), false);
        assert_eq!(cpu.flag(Flag::N), true);

        cpu.ac = 0b1000_0000;
        cpu.sr = 0x0;
        cpu.execute(&mut bus_mock, &instr);
        assert_eq!(cpu.flag(Flag::C), true);
        assert_eq!(cpu.flag(Flag::Z), true);
        assert_eq!(cpu.flag(Flag::N), false);
    }

    #[test]
    fn asl_zeropage() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ASL,
            mode: AddressMode::Zeropage(0x44),
        };

        bus_mock.read_byte
            .given(Matcher::Val(0x0044))
            .will_return(0x12);
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 5);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0044, 0x24))
        ));
    }

    #[test]
    fn asl_zeropage_x() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ASL,
            mode: AddressMode::ZeropageX(0x44),
        };

        cpu.x = 2;
        bus_mock.read_byte
            .given(Matcher::Val(0x0046))
            .will_return(0x12);
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 6);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0046, 0x24))
        ));
    }

    #[test]
    fn asl_absolute() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ASL,
            mode: AddressMode::Absolute(0x1234),
        };

        bus_mock.read_byte
            .given(Matcher::Val(0x1234))
            .will_return(0x12);
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 6);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x1234, 0x24))
        ));
    }

    #[test]
    fn asl_absolute_x() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::ASL,
            mode: AddressMode::AbsoluteX(0x1234),
        };

        cpu.x = 2;
        bus_mock.read_byte
            .given(Matcher::Val(0x1236))
            .will_return(0x12);
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 7);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x1236, 0x24))
        ));
    }

    #[test]
    fn lsr_accumulator() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::LSR,
            mode: AddressMode::Accumulator,
        };

        cpu.ac = 0xAF;
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.ac, 0x57);
    }

    #[test]
    fn lsr_accumulator_flags() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::LSR,
            mode: AddressMode::Accumulator,
        };

        cpu.ac = 0b0000_0001;
        cpu.sr = 0x0;
        cpu.execute(&mut bus_mock, &instr);
        assert_eq!(cpu.flag(Flag::C), true);
        assert_eq!(cpu.flag(Flag::Z), true);
        assert_eq!(cpu.flag(Flag::N), false);
    }

    #[test]
    fn lsr_zeropage() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::LSR,
            mode: AddressMode::Zeropage(0x44),
        };

        bus_mock.read_byte
            .given(Matcher::Val(0x0044))
            .will_return(0x12);
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 5);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0044, 0x09))
        ));
    }

    #[test]
    fn lsr_zeropage_x() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::LSR,
            mode: AddressMode::ZeropageX(0x44),
        };

        cpu.x = 2;
        bus_mock.read_byte
            .given(Matcher::Val(0x0046))
            .will_return(0x12);
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 6);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x0046, 0x09))
        ));
    }

    #[test]
    fn lsr_absolute() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::LSR,
            mode: AddressMode::Absolute(0x1234),
        };

        bus_mock.read_byte
            .given(Matcher::Val(0x1234))
            .will_return(0x12);
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 6);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x1234, 0x09))
        ));
    }

    #[test]
    fn lsr_absolute_x() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::LSR,
            mode: AddressMode::AbsoluteX(0x1234),
        };

        cpu.x = 2;
        bus_mock.read_byte
            .given(Matcher::Val(0x1236))
            .will_return(0x12);
        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 7);
        assert!(verify(
            bus_mock.write_byte
                .was_called_with((0x1236, 0x09))
        ));
    }

    #[test]
    fn clc() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CLC,
            mode: AddressMode::Implied,
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.flag(Flag::C), false);
    }

    #[test]
    fn cld() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CLD,
            mode: AddressMode::Implied,
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.flag(Flag::D), false);
    }

    #[test]
    fn cli() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CLI,
            mode: AddressMode::Implied,
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.flag(Flag::I), false);
    }

    #[test]
    fn clv() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::CLV,
            mode: AddressMode::Implied,
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.flag(Flag::V), false);
    }

    #[test]
    fn sec() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::SEC,
            mode: AddressMode::Implied,
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.flag(Flag::C), true);
    }

    #[test]
    fn sed() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::SED,
            mode: AddressMode::Implied,
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.flag(Flag::D), true);
    }

    #[test]
    fn sei() {
        let mut bus_mock = BusMock::new();
        let mut cpu = CPU::new();
        let instr = Instruction {
            mnemonic: Mnemonic::SEI,
            mode: AddressMode::Implied,
        };

        let cycles = cpu.execute(&mut bus_mock, &instr);

        assert_eq!(cycles, 2);
        assert_eq!(cpu.flag(Flag::I), true);
    }
}
