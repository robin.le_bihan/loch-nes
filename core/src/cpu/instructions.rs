use std::fmt;

#[derive(PartialEq, Eq, Debug)]
/// Encapsulate the different address modes for a single 6502 instruction.
pub enum AddressMode {
    /// `OPC A`: operand is AC (implied single byte instruction)
    Accumulator,

    /// `OPC $LLHH`: operand is address `$HHLL`
    Absolute(u16),
    /// `OPC $LLHH,X`: operand is address; effective address is address incremented by `X` with carry
    AbsoluteX(u16),
    /// `OPC $LLHH,Y`: operand is address; effective address is address incremented by `Y` with carry
    AbsoluteY(u16),

    /// `OPC #$BB`: operand is byte `BB`
    Immediate(u8),

    /// `OPC`: operand implied
    Implied,

    /// `OPC ($LLHH)`: operand is address; effective address is contents of word at address: `C.w($HHLL)`
    Indirect(u16),
    /// `OPC ($LL,X)`: operand is zeropage address; effective address is word in (LL + X, LL + X + 1), inc. without carry: `C.w($00LL + X)`
    IndirectX(u8),
    /// `OPC ($LL),Y`: operand is zeropage address; effective address is word in (LL, LL + 1) incremented by Y with carry: `C.w($00LL) + Y`
    IndirectY(u8),

    /// `OPC $BB`: branch target is PC + signed offset BB
    Relative(u8),

    /// `OPC $LL`: operand is zeropage address (hi-byte is zero, address = $00LL)
    Zeropage(u8),
    /// `OPC $LL,X`: operand is zeropage address; effective address is address incremented by X without carry
    ZeropageX(u8),
    /// `OPC $LL,Y`: operand is zeropage address; effective address is address incremented by Y without carry
    ZeropageY(u8),
}

#[derive(PartialEq, Eq, Debug)]
/// Represents an 6502 instruction mnemonic.
pub enum Mnemonic {
    /// add with carry
    ADC,
    /// and (with accumulator)
    AND,
    /// arithmetic shift left
    ASL,
    /// branch on carry clear
    BCC,
    /// branch on carry set
    BCS,
    /// branch on equal (zero set)
    BEQ,
    /// bit test
    BIT,
    /// branch on minus (negative set)
    BMI,
    /// branch on not equal (zero clear)
    BNE,
    /// branch on plus (negative clear)
    BPL,
    /// break / interrupt
    BRK,
    /// branch on overflow clear
    BVC,
    /// branch on overflow set
    BVS,
    /// clear carry
    CLC,
    /// clear decimal
    CLD,
    /// clear interrupt disable
    CLI,
    /// clear overflow
    CLV,
    /// compare (with accumulator)
    CMP,
    /// compare with X
    CPX,
    /// compare with Y
    CPY,
    /// decrement
    DEC,
    /// decrement X
    DEX,
    /// decrement Y
    DEY,
    /// exclusive or (with accumulator)
    EOR,
    /// increment
    INC,
    /// increment X
    INX,
    /// increment Y
    INY,
    /// jump
    JMP,
    /// jump subroutine
    JSR,
    /// load accumulator
    LDA,
    /// load X
    LDX,
    /// load Y
    LDY,
    ///.ogical shift right
    LSR,
    /// no operation
    NOP,
    /// or with accumulator
    ORA,
    /// push accumulator
    PHA,
    /// push processor status (SR)
    PHP,
    /// pull accumulator
    PLA,
    /// pull processor status (SR)
    PLP,
    /// rotate left
    ROL,
    /// rotate right
    ROR,
    /// return from interrupt
    RTI,
    /// return from subroutine
    RTS,
    /// subtract with carry
    SBC,
    /// set carry
    SEC,
    /// set decimal
    SED,
    /// set interrupt disable
    SEI,
    /// store accumulator
    STA,
    /// store X
    STX,
    /// store Y
    STY,
    /// transfer accumulator to X
    TAX,
    /// transfer accumulator to Y
    TAY,
    /// transfer stack pointer to X
    TSX,
    /// transfer X to accumulator
    TXA,
    /// transfer X to stack pointer
    TXS,
    /// transfer Y to accumulator
    TYA,

    /// End of Program instruction.
    /// Exclusively reserved for emulator implementation.
    XXX,
}

#[derive(PartialEq, Eq, Debug)]
/// Represents a complete instruction (mnemonic + address mode)
pub struct Instruction {
    pub mnemonic: Mnemonic,
    pub mode: AddressMode,
}

impl Instruction {
    /// Constructor.
    pub fn new(mnemonic: Mnemonic, mode: AddressMode) -> Self {
        Instruction {
            mnemonic,
            mode,
        }
    }

    pub fn is_eop(&self) -> bool {
        self.mnemonic == Mnemonic::XXX
    }

    pub fn size(&self) -> u8 {
        use AddressMode::*;

        1 + match self.mode {
            Implied | Accumulator => 0,

            Absolute(_) | AbsoluteX(_) | AbsoluteY(_)
            | Immediate(_) | Indirect(_) => 2,

            Relative(_) | Zeropage(_) | ZeropageX(_)
            | ZeropageY(_) | IndirectX(_) | IndirectY(_) => 1,
        }
    }
}

impl std::fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Mnemonic::*;
        use AddressMode::*;

        write!(
            f, "{} {}",
            match self.mnemonic {
                ADC => "ADC",
                AND => "AND",
                ASL => "ASL",
                BCC => "BCC",
                BCS => "BCS",
                BEQ => "BEQ",
                BIT => "BIT",
                BMI => "BMI",
                BNE => "BNE",
                BPL => "BPL",
                BRK => "BRK",
                BVC => "BVC",
                BVS => "BVS",
                CLC => "CLC",
                CLD => "CLD",
                CLI => "CLI",
                CLV => "CLV",
                CMP => "CMP",
                CPX => "CPX",
                CPY => "CPY",
                DEC => "DEC",
                DEX => "DEX",
                DEY => "DEY",
                EOR => "EOR",
                INC => "INC",
                INX => "INX",
                INY => "INY",
                JMP => "JMP",
                JSR => "JSR",
                LDA => "LDA",
                LDX => "LDX",
                LDY => "LDY",
                LSR => "LSR",
                NOP => "NOP",
                ORA => "ORA",
                PHA => "PHA",
                PHP => "PHP",
                PLA => "PLA",
                PLP => "PLP",
                ROL => "ROL",
                ROR => "ROR",
                RTI => "RTI",
                RTS => "RTS",
                SBC => "SBC",
                SEC => "SEC",
                SED => "SED",
                SEI => "SEI",
                STA => "STA",
                STX => "STX",
                STY => "STY",
                TAX => "TAX",
                TAY => "TAY",
                TSX => "TSX",
                TXA => "TXA",
                TXS => "TXS",
                TYA => "TYA",
                XXX => "XXX",
            },
            match self.mode {
                Accumulator => "A".to_string(),
                Absolute(x) => format!("${:04X}", x),
                AbsoluteX(x) => format!("${:04X},X", x),
                AbsoluteY(x) => format!("${:04X},Y", x),
                Immediate(x) => format!("#${:02X}", x),
                Implied => "".to_string(),
                Indirect(x) => format!("(${:04X})", x),
                IndirectX(x) => format!("(${:02X},X)", x),
                IndirectY(x) => format!("(${:02X}),Y", x),
                Relative(x) => format!("${:02X}", x),
                Zeropage(x) => format!("${:02X}", x),
                ZeropageX(x) => format!("${:02X},X", x),
                ZeropageY(x) => format!("${:02X},Y", x),
            }
        )
    }
}
