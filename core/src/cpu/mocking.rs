pub use mock_it::{Matcher, Mock, verify};

use crate::bus::Bus;

pub struct BusMock {
    pub read_byte: Mock<Matcher<u16>, u8>,
    pub write_byte: Mock<(u16, u8), ()>,
    pub read_word: Mock<Matcher<u16>, u16>,
    pub write_word: Mock<(u16, u16), ()>,
}

impl BusMock {
    pub fn new() -> Self {
        BusMock {
            read_byte: Mock::new(0xFF),
            write_byte: Mock::new(()),
            read_word: Mock::new(0xFF),
            write_word: Mock::new(()),
        }
    }
}

impl Bus for BusMock {
    fn read_byte(&self, addr: u16) -> u8 {
        self.read_byte.called(Matcher::Val(addr))
    }

    fn write_byte(&mut self, addr: u16, byte: u8) {
        self.write_byte.called((addr, byte));
    }

    fn read_word(&self, addr: u16) -> u16 {
        self.read_word.called(Matcher::Val(addr))
    }

    fn write_word(&mut self, addr: u16, word: u16) {
        self.write_word.called((addr, word));
    }
}
