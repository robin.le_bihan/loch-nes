//! This module aims to emulate a 6502 chip.

use std::fmt;

use crate::bus::Bus;

mod decode;
mod execute;
mod instructions;
mod status_register;

#[cfg(test)]
mod mocking;

/// Represents the 6502 CPU.
pub struct CPU {
    /// Program counter.
    pc: u16,
    /// Accumulator register.
    ac: u8,
    /// X general purpose register.
    x: u8,
    /// Y general purpose register.
    y: u8,
    /// Status register.
    sr: u8,
    /// Stack pointer.
    sp: u8,

    remaining_cycles: u8,
    total_cycles: u32,
}

impl CPU {
    /// Default constructor.
    pub fn new() -> Self {
        CPU {
            pc: 0x0,
            ac: 0x0,
            x: 0x0,
            y: 0x0,
            sr: 0x0,
            sp: 0xFF,
            remaining_cycles: 0,
            total_cycles: 0,
        }
    }

    pub fn impulse<B: Bus>(&mut self, bus: &mut B) {
        if self.remaining_cycles == 0 {
            let instr = self.decode(bus).unwrap();
            self.remaining_cycles = self.execute(bus, &instr) - 1;
            self.total_cycles += 1;
        } else {
            self.remaining_cycles -= 1;
            self.total_cycles += 1;
        }
    }

    pub fn time(&self) -> u32 {
        self.total_cycles
    }

    pub fn reset<B: Bus>(&mut self, bus: &mut B) {
        self.interrupt(bus, 0xFFFC);
        self.sp = 0xFF;
        self.x = 0;
        self.y = 0;
        self.sr = 0 | (1 << 5);
        self.total_cycles = 0;
    }

    pub fn irq<B: Bus>(&mut self, bus: &mut B) {
        if !self.flag(status_register::Flag::I) {
            self.interrupt(bus, 0xFFFE);
        }
    }

    pub fn nmi<B: Bus>(&mut self, bus: &mut B) {
        self.interrupt(bus, 0xFFFA);
    }
}

impl fmt::Display for CPU {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use crate::cpu::status_register::Flag;

        write!(f, "PC: {:#06X}\n", self.pc)?;
        write!(f, "AC: {:#04X}\n", self.ac)?;
        write!(f, "SP: {:#04X}\n", self.sp)?;
        write!(f, "X : {:#04X}\n", self.x)?;
        write!(f, "Y : {:#04X}\n\n", self.y)?;

        write!(f, "N V B D I Z C\n")?;
        write!(f, "{} {} {} {} {} {} {}\n",
               self.flag(Flag::N) as u8,
               self.flag(Flag::V) as u8,
               self.flag(Flag::B) as u8,
               self.flag(Flag::D) as u8,
               self.flag(Flag::I) as u8,
               self.flag(Flag::Z) as u8,
               self.flag(Flag::C) as u8)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn default_ctor() {
        let cpu = CPU::new();
        assert_eq!(cpu.pc, 0x0);
        assert_eq!(cpu.ac, 0x0);
        assert_eq!(cpu.x, 0x0);
        assert_eq!(cpu.y, 0x0);
        assert_eq!(cpu.sr, 0x0);
        assert_eq!(cpu.sp, 0xFF);
    }
}
