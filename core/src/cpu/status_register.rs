use crate::cpu::CPU;

/// Represents the differents flags of the status register.
/// Flag 0010 0000 is ignored.
pub enum Flag {
    /// Negative.
    N = 0b1000_0000,
    /// Overflow.
    V = 0b0100_0000,
    /// Break.
    B = 0b0001_0000,
    /// Decimal.
    D = 0b0000_1000,
    /// Interrupt.
    I = 0b0000_0100,
    /// Zero.
    Z = 0b0000_0010,
    /// Carry.
    C = 0b0000_0001,
}

impl CPU {
    /// Get specified flag from the status register.
    pub fn flag(&self, flag: Flag) -> bool {
        let shift = match flag {
            Flag::N => 7,
            Flag::V => 6,
            Flag::B => 4,
            Flag::D => 3,
            Flag::I => 2,
            Flag::Z => 1,
            Flag::C => 0,
        };
        (self.sr & (flag as u8)) >> shift != 0
    }

    /// Set specified flag in the status register.
    pub fn set_flag(&mut self, flag: Flag, value: bool) {
        if value {
            self.sr |= flag as u8;
        } else {
            self.sr &= !(flag as u8);
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn n_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0b1000_0000;
        assert_eq!(cpu.flag(Flag::N), true);
    }

    #[test]
    fn set_n_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0;
        cpu.set_flag(Flag::N, true);
        assert_eq!(cpu.sr, 0b1000_0000);
    }

    #[test]
    fn v_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0b0100_0000;
        assert_eq!(cpu.flag(Flag::V), true);
    }

    #[test]
    fn set_v_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0xFF;
        cpu.set_flag(Flag::V, false);
        assert_eq!(cpu.sr, 0b1011_1111);
    }

    #[test]
    fn b_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0b1110_1111;
        assert_eq!(cpu.flag(Flag::B), false);
    }

    #[test]
    fn set_b_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0x00;
        cpu.set_flag(Flag::B, true);
        assert_eq!(cpu.sr, 0b0001_0000);
    }

    #[test]
    fn d_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0b0000_1000;
        assert_eq!(cpu.flag(Flag::D), true);
    }

    #[test]
    fn set_d_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0x00;
        cpu.set_flag(Flag::D, true);
        assert_eq!(cpu.sr, 0b0000_1000);
    }

    #[test]
    fn i_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0b1111_1011;
        assert_eq!(cpu.flag(Flag::I), false);
    }

    #[test]
    fn set_i_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0x00;
        cpu.set_flag(Flag::I, true);
        assert_eq!(cpu.sr, 0b0000_0100);
    }

    #[test]
    fn z_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0b0000_0010;
        assert_eq!(cpu.flag(Flag::Z), true);
    }

    #[test]
    fn set_z_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0x00;
        cpu.set_flag(Flag::Z, true);
        assert_eq!(cpu.sr, 0b0000_0010);
    }

    #[test]
    fn c_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0b0000_0001;
        assert_eq!(cpu.flag(Flag::C), true);
    }

    #[test]
    fn set_c_flag() {
        let mut cpu = CPU::new();
        cpu.sr = 0x01;
        cpu.set_flag(Flag::C, false);
        assert_eq!(cpu.sr, 0b0000_0000);
    }
}
