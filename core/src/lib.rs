pub mod bus;
pub mod cpu;
pub mod memory;
pub mod cartridge;

pub struct NES {
    cpu: cpu::CPU,
    bus: bus::cpu::CPUBus,
}

impl NES {
    pub fn new() -> Self {
        NES {
            cpu: cpu::CPU::new(),
            bus: bus::cpu::CPUBus::new(),
        }
    }

    pub fn clock(&mut self) {
        self.cpu.impulse(&mut self.bus);
    }

    pub fn insert_cartridge(&mut self, cartridge: cartridge::Cartridge) {
        self.bus.connect_cartridge(cartridge);
    }

    pub fn power_on(&mut self) {
        self.cpu.reset(&mut self.bus);
    }
}
