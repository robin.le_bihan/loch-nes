//! This module aims to emulate NES memory components that can be
//! connected to the data bus.

pub mod wram;

pub trait ReadMemory {
    fn read(&self, address: u16) -> u8;
}

pub trait WriteMemory {
    fn write(&mut self, address: u16, byte: u8);
}
