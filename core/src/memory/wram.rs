//! This module aims to simulate the work RAM of the NES.
use std::fmt;

use crate::memory::{ReadMemory, WriteMemory};

const WRAM_SIZE: usize = 0x800;

/// The memory of the NES consists of 
pub struct WorkRAM {
    mem: [u8; WRAM_SIZE],
}

impl WorkRAM {
    pub fn new() -> Self {
        WorkRAM {
            mem: [0x0; WRAM_SIZE]
        }
    }
}

impl fmt::Display for WorkRAM {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "    ")?;
        for i in 0..8 {
            write!(f, "  {:X} {:X}", i * 2, i * 2 + 1)?;
        }
        write!(f, "\n")?;

        for i in 0..32 {
            write!(f, "{:#05X}", i * 16)?;

            for j in 0..8 {
                write!(f, " {:02x}{:02x}",
                       self.mem[i * 16 + (j * 2)],
                       self.mem[i * 16 + (j * 2 + 1)])?;
            }

            write!(f, "\n")?;
        }

        Ok(())
    }
}

impl ReadMemory for WorkRAM {
    fn read(&self, address: u16) -> u8 {
        self.mem[address as usize & 0x7FF]
    }
}

impl WriteMemory for WorkRAM {
    fn write(&mut self, address: u16, byte: u8) {
        self.mem[address as usize & 0x7FF] = byte;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn read_main_ram() {
        let mut ram = WorkRAM::new();
        ram.mem[0x123] = 0x23;
        assert_eq!(ram.read(0x123), 0x23);
    }

    #[test]
    fn read_mirror_ram() {
        let mut ram = WorkRAM::new();
        ram.mem[0x56] = 0x12;
        assert_eq!(ram.read(0x856), 0x12);
    }

    #[test]
    fn write_main_ram() {
        let mut ram = WorkRAM::new();
        ram.write(0x234, 0x24);
        assert_eq!(ram.mem[0x234], 0x24);
    }

    #[test]
    fn write_mirror_ram() {
        let mut ram = WorkRAM::new();
        ram.write(0x932, 0x17);
        assert_eq!(ram.mem[0x132], 0x17);
    }
}
