extern crate clap;

use clap::{App, Arg};

static ASCII_ART: &'static str = "  _               _       _   _ _____ ____
 | |    ___   ___| |__   | \\ | | ____/ ___|
 | |   / _ \\ / __| '_ \\  |  \\| |  _| \\___ \\ 
 | |__| (_) | (__| | | | | |\\  | |___ ___) |
 |_____\\___/ \\___|_| |_| |_| \\_|_____|____/ 
";

fn main() {
    let matches = App::new("Loch NES")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Robin Le Bihan")
        .about("NES emulator and tools")
        .get_matches();

    println!("{}", ASCII_ART);
}
